package org.idk.newsagency.repository;

import org.idk.newsagency.entity.Announcement;
import org.springframework.stereotype.Repository;

@Repository
public interface AnnouncementRepository extends AbstractRepository<Announcement> {
}
