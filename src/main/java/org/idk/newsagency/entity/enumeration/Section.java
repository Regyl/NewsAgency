package org.idk.newsagency.entity.enumeration;

import lombok.Getter;

@Getter
public enum Section {
    ENVIRONMENT("Окружение"),
    CAREER("Карьера"),
    LOVE("Любовь"),
    SELF_DEV("Саморазвитие"),
    HEALTH("Здоровье"),
    VOLUNTEER("Благотворение"),
    FINANCE("Финансы"),
    REST("Отдых");

    private final String translation;

    Section(String translation) {
        this.translation = translation;
    }
}
