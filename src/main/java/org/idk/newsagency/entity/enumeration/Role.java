package org.idk.newsagency.entity.enumeration;

public enum Role {
    ADMIN,
    MODERATOR,
    USER;
}
